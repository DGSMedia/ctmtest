const count = require('./count');

test(' the string : "hello \n world \r! " should be equal to "Hello world !"', () => {
    expect(count.parseContent('hello \n world \r! ')).toBe('hello  world !');
});

test(' if a number it`s a prime number should be equal to true', () => {
    expect(count.isPrimeNumber(2)).toBe(true);
    expect(count.isPrimeNumber(4)).toBe(false);
    expect(count.isPrimeNumber(5)).toBe(true);
    expect(count.isPrimeNumber(1553535335353)).toBe(false);
});


test(' sortByKey it`s ordering the arry by the selected key' , () => {
    let test = [ 
        { count: 2, word: 'bow' },
        { count: 1, word: 'sword' },
        { count: 5, word: 'axe' } ];
    let resultDesc = [ 
        { count: 5, word: 'axe' } ,
        { count: 2, word: 'bow' },
        { count: 1, word: 'sword' }
    ];
    let resultAsc = [ 
        { count: 1, word: 'sword'},
        { count: 2, word: 'bow' },
        { count: 5, word: 'axe' }
    ];
    expect(count.sortByKey(test,'count',-1)).toEqual(resultDesc);
    expect(count.sortByKey(test,'count',1)).toEqual(resultAsc);
});


test('getwordsCountList return an array of object for each word', () => {
    let content = 'this is a simple 3 !?    content text ... for test';
    let result =  [ 
        { count: 1, word: 'this' },
        { count: 1, word: 'is' },
        { count: 1, word: 'a' },
        { count: 1, word: 'simple' },
        { count: 1, word: 'content' },
        { count: 1, word: 'text' },
        { count: 1, word: 'for' },
        { count: 1, word: 'test' } ];
    expect(count.getWordsCountList(content)).toEqual(result);
});