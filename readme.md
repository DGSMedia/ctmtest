
## Getting Started

clone this repository on your local environment by using:
```
git clone https://Degrand91@bitbucket.org/Degrand91/ctmtest.git
```

### Installing

For run this application you have to install some dipendency first so run :

```
npm install
```

## Running the tests

The tests are made using "jest" and for run them just run this command on your terminal
```
npm test
```
the file count.test.js will be processed and run 4 tests to ensure the app functionality

## Running the app

to run the app simply execute on your terminal the following command
```
npm start
```

to see the final result just visit

```
http://localhost:3002
```

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details