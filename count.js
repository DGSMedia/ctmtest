

const parseContent = content => {
    let parsed = content.replace(/\r?\n|\r/g,'').trim();
    return parsed;
};

const getWordsCountList = content => {
    let result = [];
    let wordsObj = {};
    let wordsList = content.match(/\b[^\d\W]+\b/g);
    wordsList.forEach(word => {
        wordsObj[word] = wordsObj[word] || {count:0, word : word};
        wordsObj[word].count++;
    });
    Object.keys(wordsObj).forEach(word => {
        result.push(wordsObj[word]);
    });
    return result;
};

const isPrimeNumber = num => {
    for(let i = 2, s = Math.sqrt(num); i <= s; i++)
        if(num % i === 0) return false; 
    return num !== 1;
};

const sortByKey = (array, key, order=1) => {
    return array.sort((a, b) => {
        let x = a[key];
        let y = b[key];
        return ((x < y) ? -1 : ((x > y) ? 1 : 0)) * order;
    });
};

const buildResult = wordsList =>{
    let result = {
        primes: [],
        words: []
    };
 
    let sortedList = sortByKey(wordsList,'count',-1);
    sortedList.forEach(wordsObj => {
        let count = wordsObj.count;
        let isPrime = wordsObj.isPrime = isPrimeNumber(count);
        if (isPrime) {
            result.primes.push(wordsObj.word);
        }
        result.words.push(wordsObj);
    });
    return result;
};



module.exports = {
    parseContent: parseContent,
    getWordsCountList:getWordsCountList,
    isPrimeNumber:isPrimeNumber,
    sortByKey:sortByKey,
    buildResult:buildResult
};
