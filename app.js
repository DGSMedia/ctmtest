
const fetch = require('node-fetch');
const count = require('./count');
const express = require('express');
const app = express();

app.get('/', start);
//app.get('/', (req, res) => res.send('Hello World!'))

app.listen(3002);


function start(req, res){
    let url = 'http://www.loyalbooks.com/download/text/Railway-Children-by-E-Nesbit.txt';
    fetch(url)
        .catch(res.send)
        .then(response => response.text())
        .then(count.parseContent)
        .then(count.getWordsCountList)
        .then(count.buildResult)
        .then((result)=>res.send(result));
}